//+------------------------------------------------------------------+
//|                                                       MetaTrader |
//|                   Copyright 2001-2014, MetaQuotes Software Corp. |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
#include "StdAfx.h"
#include "service/service.h"
#include "lib/Log.h"

int _tmain(int argc, TCHAR *argv[])
{
	LoggerObject log;

	log << "Starting service";
	// Start the the service from service.h, which start CollecktTicks.h, which collects the ticks.

	SERVICE_TABLE_ENTRY ServiceTable[] =
	{
		{ SERVICE_NAME, (LPSERVICE_MAIN_FUNCTION)ServiceMain },
		{ NULL, NULL }
	};

	if (StartServiceCtrlDispatcher(ServiceTable) == FALSE)
	{
		log << "Service failed to start. ERROR: " << GetLastError();
		return GetLastError();
	}

	log << "Service Stopped";
	return 0;
}