#pragma once
#include "StdAfx.h"
#include "windows.h"
#include <fstream>
#include <string>

using namespace std;

class LoggerObject {
private:
	bool newfile;
	bool newDay;
	TCHAR LogPath[MAX_PATH];
	char date[80];
	char dateTime[80];
	char latestObtainedDay[4];
	char currentDay[4];
	ofstream logfile;

	void setPath();
	void setDates();
	void newLogFile();
public:
	LoggerObject();
	~LoggerObject();
	template <typename T>
	ostream& operator << (T arg) {
		this->setDates();
		logfile << endl << string(dateTime) << ' ' << arg;
		logfile.flush();
		return logfile;
	}
};