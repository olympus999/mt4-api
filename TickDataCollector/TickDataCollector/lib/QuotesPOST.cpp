#include "StdAfx.h"
#include <ctime>
#include <iostream>
#include <string> 
#include <sstream>
#include <fstream>
#include <windows.h>
#include <assert.h>
#include <mutex>
#include <queue>
#include <WinBase.h>
#include <map>

//--- Custom
#include "QuotesPOST.h"
#include "MT4ManagerAPI.h"
#include "CManager.h"
#include "minIni.h"
#include "curl/curl.h"
//----

#define sizearray(a)  (sizeof(a) / sizeof((a)[0]))


// Switch to pumping
void __stdcall getTicksInPumping(int code, int type, void* data, void* parm)
{
	Quotes *yc_ptr = (Quotes*)parm;
	yc_ptr->MT4CALLBACK_SYMBOL_PRICE(code);
	return;
}
// Start new thread
DWORD __stdcall threadPostQuotes(LPVOID arg)
{
	if (!arg)
		return 0;
	Quotes *yc_ptr = (Quotes*)arg;
	//yc_ptr->curl(&yc_ptr->queueSymbolInfo);
	return 1;
}

Quotes::Quotes()
{
	// Flag "true" allows service to run, false instructs service to stop
	flag = true;

	GetModuleFileName(NULL, path, MAX_PATH);
	for (int i = MAX_PATH; i >= 0; i--) {
		if (path[i] == '\\') {
			path[i] = '\0';
			break;
		}
	}
}

int Quotes::start()
{
	//--- check MT4 API creation
	if (manager.IsValid() == FALSE)
	{
		log << "INCORRECT VERSION. Probably using old .dll for MT4 API";
		return(-1);
	}
	//--- Start connection, prepare symbols and switch to pumping. Also use in case of reconnect
	prepare();
	//--- 
	//--- Loop
	// IF flag is false, then service is requested to stop
	while (flag)
	{
		Sleep(10000);
		//--- Check if connection lost, if needed, reconnect
		checkConnection();
	}
	return 0;
}

void Quotes::prepare()
{
	//--- connect
	connect();
	//--- update local symbols cache
	updateCache();
	//--- Register all symbols for future ticks
	registerSymbols();
	//--- Switch to pumping
	switchPumping();
}

// Connect to MT4 Server
int Quotes::connect()
{
	char server[100], password[100];
	long login;
	int res = RET_ERROR;
	//--- Read .ini file
	TCHAR inifile[MAX_PATH];
	_tcscpy_s(inifile, MAX_PATH, path);
	strcat(inifile, "\\conf.ini");
	ini_gets("mt4", "server", "ERROR", server, sizearray(server), inifile);
	login = ini_getl("mt4", "login", 0, inifile);
	ini_gets("mt4", "password", "ERROR", password, sizearray(password), inifile);
	//--- Connect
	while (res)
	{
		if ((res = manager->Connect(server)) != RET_OK || (res = manager->Login(login, password)) != RET_OK)
		{
			log << "Erro MT4 Server login. Error description: " << manager->ErrorDescription(res);
		}
		else if (res = manager->IsConnected() != 0)
		{
			log << "Connected to MT4 Server";
			break;
		}
	}
	return 0;
}
// Update local symbols cache
int Quotes::updateCache()
{
	int res = RET_ERROR;
	res = manager->SymbolsRefresh();
	if (res != RET_OK)
	{
		log << "Error with function SymbolsRefresh: " << manager->ErrorDescription(res);
		return(-1);
	}

	return 0;
}
// Register symbols
int Quotes::registerSymbols()
{
	ConSymbol *allSymbols = NULL;
	int res = RET_ERROR;
	int i, total;
	//--- Get all data for all symbols
	allSymbols = manager->SymbolsGetAll(&total);
	if (allSymbols == NULL)
	{
		log << "Error with function allSymbols: " << manager->ErrorDescription(res);
		return(-1);
	}
	//--- Register all symbols for future ticks
	for (i = 0; i < total; i++)
	{
		res = manager->SymbolAdd(allSymbols[i].symbol);
		if (res != 0)
		{
			log << "Error with adding a symbol. Symbol: " << allSymbols[i].symbol << "Erro code: " << res << " Error: " << manager->ErrorDescription(res);
		}
	}
	//
	log << "Total symbols = " << total;
	//free memory
	manager->MemFree(allSymbols);

	return 0;
}
int Quotes::switchPumping()
{
	int res = RET_ERROR;
	HWND hd = NULL;
	UINT ut = 0;
	int flags = 0;
	// First parameter is a function which will be called in case of data is updated in server
	res = manager->PumpingSwitchEx(getTicksInPumping, flags, this);
	if (res != 0)
	{
		log << "Error Unable to switch to pumping. Error: " << manager->ErrorDescription(res);
		return -1;
	}

	return 0;
}
// Store ticks in queue
void __stdcall Quotes::MT4CALLBACK_SYMBOL_PRICE(int code)
{
	int total, i;
	SymbolInfo si[2057];
	if (PUMP_UPDATE_BIDASK == code)
	{
		total = manager->SymbolInfoUpdated(si, 2056);
		m.lock();
		for (i = 0; i < total; i++)
		{
			queueSymbolInfo.push(si[i]);
		}
		m.unlock();
	}
	this->postQuotes();
}
// Write to file
int Quotes::postQuotes()
{
	//HANDLE thread = CreateThread(NULL, 0, threadPostQuotes, this, 0, NULL);
	this->curl();
	return 0;
}
// send quotes
DWORD WINAPI Quotes::curl() {

	// PREPARE DATA FOR CURL

	SymbolInfo s;
	std::queue<SymbolInfo> copy;
	typedef map<string, SymbolInfo> SymbolArray;
	SymbolArray quotesLatest;
	std::string postData = "";

	m.lock();
	copy = queueSymbolInfo;
	while (!queueSymbolInfo.empty())
	{
		queueSymbolInfo.pop();
	}
	m.unlock();

	while (!copy.empty())
	{
		// Get symbol from queue and pop one out
		s = copy.front();
		quotesLatest[s.symbol] = s;
		copy.pop();
	}

	for (auto const &ent1 : quotesLatest) {
		postData = ent1.first + "=" + std::to_string(ent1.second.bid);
		ent1.first; // key
		ent1.second; // data
	}

	log << "postData = " << postData;

	// CURL
	CURL *curl;
	CURLcode res;

	/* In windows, this will init the winsock stuff */
	curl_global_init(CURL_GLOBAL_ALL);

	/* get a curl handle */
	curl = curl_easy_init();
	if (curl) {
		/* First set the URL that is about to receive our POST. This URL can
		just as well be a https:// URL if that is what should receive the
		data. */
		curl_easy_setopt(curl, CURLOPT_URL, "http://test.100moneta.com/post");
		/* Now specify the POST data */
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postData);

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);
		/* Check for errors */
		if (res != CURLE_OK)
			log << "curl_easy_perform() failed: " << curl_easy_strerror(res);

		/* always cleanup */
		curl_easy_cleanup(curl);
	}
	curl_global_cleanup();

	return 0;
}
// Check connection and reconnect
int Quotes::checkConnection()
{
	int res = RET_ERROR;
	if (res = manager->IsConnected() == 0)
	{
		log << "Error Lost connection, reconnecting. Error: " << manager->ErrorDescription(res);
		prepare();
	}

	return 0;
}

int Quotes::verifyMT4API()
{
	if (manager.IsValid() == FALSE)
	{
		log << "INCORRECT VERSION. Probably using old .dll for MT4 API";
		return(-1);
	}
	return 0;
}

int Quotes::stop()
{
	flag = false;
	return 0;
}