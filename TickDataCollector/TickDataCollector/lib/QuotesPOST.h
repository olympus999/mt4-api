#pragma once

#include <ctime>
#include <iostream>
#include <string> 
#include <sstream>
#include <fstream>
#include <windows.h>
#include <mutex>
#include <queue>
//--- Custom
#include "MT4ManagerAPI.h"
#include "Log.h"
//----

DWORD __stdcall startThreadWriteData(LPVOID arg);
void __stdcall getTicksInPumping(int code, int type, void* data, void* parm);

class Quotes {
private:
	std::mutex m;
	bool flag;
	TCHAR path[MAX_PATH];
	LoggerObject log;
public:
	std::queue<SymbolInfo> queueSymbolInfo;
	Quotes();
	int start();
	int stop();
	DWORD WINAPI curl();
	void __stdcall MT4CALLBACK_SYMBOL_PRICE(int);
private:
	int connect();
	int updateCache();
	int registerSymbols();
	int switchPumping();
	int postQuotes();
	int checkConnection();
	void prepare();
	int verifyMT4API();
};