#include "StdAfx.h"
#include <iostream>
#include <fstream>
#include <time.h>
#include "Log.h"

using namespace std;

LoggerObject::LoggerObject() {
	newfile = true;
	newDay = false;
	// Set random values which are not equal
	latestObtainedDay[0] = '2';
	currentDay[0] = '3';
}
// Set/Update variables date, dateTime and latestDay
void LoggerObject::setDates() {
	time_t     now = time(0);
	struct tm  tstruct;
	tstruct = *localtime(&now);
	strftime(date, sizeof(date), "%Y.%m.%d-%a", &tstruct);
	strftime(dateTime, sizeof(dateTime), "%Y.%m.%d-%H:%M:%S", &tstruct);
	// Check if day has changed and adapt accordingly
	strftime(currentDay, sizeof(currentDay), "%a", &tstruct);
	if (currentDay[0] != latestObtainedDay[0]) {
		strftime(latestObtainedDay, sizeof(latestObtainedDay), "%a", &tstruct);
		newLogFile();
	}
}
void LoggerObject::newLogFile() {
	this->setPath();
	logfile.close();
	logfile.open(LogPath, ios::app);
	logfile.rdbuf()->pubsetbuf(0, 0);
}
// Set/Update logfile path and logfile name (incl date)
void LoggerObject::setPath() {
	GetModuleFileName(NULL, LogPath, MAX_PATH);
	for (int i = MAX_PATH; i >= 0; i--) {
		if (LogPath[i] == '\\') {
			LogPath[i] = '\0';
			break;
		}
	}
	strcat(LogPath, "\\logs");
	CreateDirectory(LogPath, NULL);
	strcat(LogPath, "\\");
	strcat(LogPath, date);
	strcat(LogPath, ".log");
}
LoggerObject::~LoggerObject() {
	logfile.close();
}