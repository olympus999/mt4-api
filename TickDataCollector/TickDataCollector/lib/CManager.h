#pragma once

class CManager
{
private:
	CManagerFactory   m_factory;
	CManagerInterface *m_manager;
public:
	CManager() : m_factory("..\\TickDataCollector\\lib\\mtmanapi.dll"),
		m_manager(NULL)
	{
		m_factory.WinsockStartup();
		if (m_factory.IsValid() == FALSE || (m_manager = m_factory.Create(ManAPIVersion)) == NULL)
		{
			if (m_manager == NULL) printf("m_manager is null");
			printf("Failed to create manager interface\n");
			Sleep(1000);
			return;
		}
	}
	~CManager()
	{
		if (m_manager != NULL) { m_manager->Release(); m_manager = NULL; }
		m_factory.WinsockCleanup();
	}
	bool              IsValid() { return(m_manager != NULL); }
	CManagerInterface* operator->() { return(m_manager); }
};

CManager manager;