#pragma once
#include <Windows.h>
#include <tchar.h>

#define SERVICE_NAME  _T("")

VOID WINAPI ServiceMain(DWORD argc, LPTSTR *argv);
VOID WINAPI ServiceCtrlHandler(DWORD);
DWORD WINAPI ServiceWorkerThread(LPVOID lpParam);
